package content

import (
	"net/http"
)

func init() {
	http.HandleFunc("/frontend.js", func(res http.ResponseWriter, req *http.Request) {
		http.ServeFile(res, req, "./content/frontend.js")
	})
	http.HandleFunc("/frontend_bg.wasm", func(res http.ResponseWriter, req *http.Request) {
		http.ServeFile(res, req, "./content/frontend_bg.wasm")
	})
	http.HandleFunc("/frontend_bg", func(res http.ResponseWriter, req *http.Request) {
		http.ServeFile(res, req, "./content/frontend_bg.wasm")
	})
	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		http.ServeFile(res, req, "./content/index.html")
	})
}
