package content

import (
	"fmt"

	"github.com/ponzu-cms/ponzu/management/editor"
	"github.com/ponzu-cms/ponzu/system/item"
)

type Post struct {
	item.Item

	Title string `json:"title"`
	Text  string `json:"text"`
}

// MarshalEditor writes a buffer of html to edit a Post within the CMS
// and implements editor.Editable
func (p *Post) MarshalEditor() ([]byte, error) {
	view, err := editor.Form(p,
		// Take note that the first argument to these Input-like functions
		// is the string version of each Post field, and must follow
		// this pattern for auto-decoding and auto-encoding reasons:
		editor.Field{
			View: editor.Input("Title", p, map[string]string{
				"label":       "Title",
				"type":        "text",
				"placeholder": "Enter the Title here",
			}),
		},
		editor.Field{
			View: editor.Richtext("Text", p, map[string]string{
				"label":       "Text",
				"placeholder": "Enter the Text here",
			}),
		},
	)

	if err != nil {
		return nil, fmt.Errorf("Failed to render Post editor view: %s", err.Error())
	}

	return view, nil
}

func init() {
	item.Types["Post"] = func() interface{} { return new(Post) }
}

// String defines how a Post is printed. Update it using more descriptive
// fields from the Post struct type
func (p *Post) String() string {
	return fmt.Sprintf("Post: %s", p.UUID)
}
